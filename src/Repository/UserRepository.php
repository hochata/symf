<?php

namespace App\Repository;

use PDOException;
use PDO;

class UserRepository {
  private $db_url;
  private $db_user;
  private $db_pass;

  public function __construct($url, $user, $pass) {
    $this -> db_url = $url['db_url'];
    $this -> db_user = $user['db_user'];
    $this -> db_pass = $pass['db_pass'];
  }

  private function open(): PDO {
    try {
      return new PDO($this->db_url, $this->db_user, $this->db_pass);
    } catch (PDOException $e) {
      exit($e -> getMessage());
    }
  }

  private function close($pdo) {
    $pdo = null;
  }

  private function exists(string $query): bool {
    $pdo = $this -> open();
    $st = $pdo -> prepare($query);
    $st -> execute();
    #$this -> close($pdo);
    $pdo = null;
    return !empty($st -> fetchAll());
  }

  public function contains(string $username): bool {
    return $this -> exists(
      "SELECT username FROM users WHERE username='$username';"
    );
  }

  public function insert(string $username, string $password): bool {
    if(!($this -> contains($username))) {
      $query = "INSERT INTO users (username, password) VALUES ('$username', '$password');";
      $pdo = $this -> open();
      $st = $pdo -> prepare($query);
      $res = $st -> execute();
      #$this -> close($pdo);
      $pdo = null;
      return $res;
    } else {
      exit('User already registered');
    }
  }

  public function validate(string $username, string $password): bool {
    return $this -> exists(
      "SELECT username FROM users WHERE username='$username' AND password='$password';"
    );
  }
}

?>
