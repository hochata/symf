<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController {
  /**
   * @Route("/")
   */
  public function home(SessionInterface $sess){
    if ($sess -> get('status')) {
      return $this -> render('home.html.twig', ['username' => $sess -> get('username')]);
    } else {
      return $this -> redirect('/login');
    }
  }

  /**
   * @Route("/logout")
   */
  public function logout(SessionInterface $sess) {
    $sess -> set('status', false);
    $sess -> set('username', null);
    return $this -> redirect('/login');
  }
}
?>
