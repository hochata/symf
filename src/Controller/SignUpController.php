<?php
namespace App\Controller;

use App\Entity\User;
use App\Form\Type\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SignUpController extends AbstractController {
  private UserRepository $repo;

  public function __construct(UserRepository $repo)
  {
    $this -> repo = $repo;
  }

  /**
   * @Route("/signup")
   */
  public function signup(Request $request) {
    $user = new User();
    $userForm = $this -> createForm(UserType::class, $user);
    $userForm -> handleRequest($request);

    if ($userForm -> isSubmitted() && $userForm -> isValid()) {
      if (!$this -> repo -> contains($user -> getUsername())) {
        $this -> repo -> insert($user -> getUsername(), $user -> getPassword());
      }
      return $this -> redirect('/login');
    }
    return $this -> render('signup.html.twig', ['form' => $userForm -> createView()]);

  }
}
?>
